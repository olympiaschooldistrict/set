package set

import (
	"sort"
	"strings"
)

func lowerString(in []string) (out []string) {
	for _, s := range in {
		out = append(out, strings.ToLower(s))
	}
	return
}

func mapString(s []string) (m map[string]bool) {
	m = map[string]bool{}
	for _, x := range s {
		m[x] = true
	}
	return
}

func sliceString(m map[string]bool) (s []string) {
	for x := range m {
		s = append(s, x)
	}
	return
}

// SensitiveStringIntersect takes two string slices and returns sorted slices of s1 only, both, and s2 only respectively
func SensitiveStringIntersect(s1, s2 []string) (leftOnly, both, rightOnly []string) {
	m1 := mapString(s1)
	m2 := mapString(s2)

	// 1 only
	for s := range m1 {
		if m2[s] {
			both = append(both, s)
		} else {
			leftOnly = append(leftOnly, s)
		}
	}

	// 2 only
	for s := range m2 {
		if !m1[s] {
			rightOnly = append(rightOnly, s)
		}
	}
	sort.Strings(leftOnly)
	sort.Strings(rightOnly)
	sort.Strings(both)
	return
}

// StringIntersect takes two string slices and returns lowercased sorted slices of s1 only, both, and s2 only respectively
func StringIntersect(s1, s2 []string) (leftOnly, both, rightOnly []string) {
	return SensitiveStringIntersect(lowerString(s1), lowerString(s2))
}

// StringUnique returns a lowercased slice of unique values
func StringUnique(s []string) []string {
	return sliceString(mapString(lowerString(s)))
}

// StringUnique returns a slice of the unique values from the supplied string
func SensitiveStringUnique(s []string) []string {
	return sliceString(mapString(s))
}

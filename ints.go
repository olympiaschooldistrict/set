package set

import (
	"sort"
)

func mapInt(s []int) (m map[int]bool) {
	m = map[int]bool{}
	for _, x := range s {
		m[x] = true
	}
	return
}

// IntIntersect takes two int slices and returns sorted slices of s1 only, both, and s2 only respectively
func IntIntersect(i1, i2 []int) (leftOnly, both, rightOnly []int) {
	m1 := mapInt(i1)
	m2 := mapInt(i2)

	// 1 only
	for s := range m1 {
		if m2[s] {
			both = append(both, s)
		} else {
			leftOnly = append(leftOnly, s)
		}
	}

	// 2 only
	for s := range m2 {
		if !m1[s] {
			rightOnly = append(rightOnly, s)
		}
	}
	sort.Ints(leftOnly)
	sort.Ints(rightOnly)
	sort.Ints(both)
	return
}

// IntUnion returns a slice which is a sorted union of all the passed int slices
func IntUnion(i1 []int, i2 ...[]int) (all []int) {
	m := mapInt(i1)
	for _, ia := range i2 {
		for _, i := range ia {
			m[i] = true
		}
	}
	for i := range m {
		all = append(all, i)
	}
	sort.Ints(all)
	return all
}

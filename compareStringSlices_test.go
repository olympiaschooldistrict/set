package set

import (
	"reflect"
	"testing"
)

func TestSensitiveStringIntersect(t *testing.T) {
	type args struct {
		s1 []string
		s2 []string
	}
	tests := []struct {
		name          string
		args          args
		wantLeftOnly  []string
		wantBoth      []string
		wantRightOnly []string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLeftOnly, gotBoth, gotRightOnly := SensitiveStringIntersect(tt.args.s1, tt.args.s2)
			if !reflect.DeepEqual(gotLeftOnly, tt.wantLeftOnly) {
				t.Errorf("SensitiveStringIntersect() gotLeftOnly = %v, want %v", gotLeftOnly, tt.wantLeftOnly)
			}
			if !reflect.DeepEqual(gotBoth, tt.wantBoth) {
				t.Errorf("SensitiveStringIntersect() gotBoth = %v, want %v", gotBoth, tt.wantBoth)
			}
			if !reflect.DeepEqual(gotRightOnly, tt.wantRightOnly) {
				t.Errorf("SensitiveStringIntersect() gotRightOnly = %v, want %v", gotRightOnly, tt.wantRightOnly)
			}
		})
	}
}

func TestStringIntersect(t *testing.T) {
	type args struct {
		s1 []string
		s2 []string
	}
	tests := []struct {
		name          string
		args          args
		wantLeftOnly  []string
		wantBoth      []string
		wantRightOnly []string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLeftOnly, gotBoth, gotRightOnly := StringIntersect(tt.args.s1, tt.args.s2)
			if !reflect.DeepEqual(gotLeftOnly, tt.wantLeftOnly) {
				t.Errorf("StringIntersect() gotLeftOnly = %v, want %v", gotLeftOnly, tt.wantLeftOnly)
			}
			if !reflect.DeepEqual(gotBoth, tt.wantBoth) {
				t.Errorf("StringIntersect() gotBoth = %v, want %v", gotBoth, tt.wantBoth)
			}
			if !reflect.DeepEqual(gotRightOnly, tt.wantRightOnly) {
				t.Errorf("StringIntersect() gotRightOnly = %v, want %v", gotRightOnly, tt.wantRightOnly)
			}
		})
	}
}

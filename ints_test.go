package set

import (
	"reflect"
	"testing"
)

func TestIntUnion(t *testing.T) {
	type args struct {
		i1 []int
		i2 [][]int
	}
	tests := []struct {
		name    string
		args    args
		wantAll []int
	}{
		{"empty", args{[]int{}, [][]int{}}, []int{}},
		{"oneOutofOrder", args{[]int{1, 7, 2, 3}, [][]int{}}, []int{1, 2, 3, 7}},
		{"repeats", args{[]int{1, 7, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 7, 3}, [][]int{}}, []int{1, 2, 3, 7}},
		{"multipleEmpty", args{[]int{-11, 7, 2, 2, 3}, [][]int{[]int{}, []int{}}}, []int{-11, 2, 3, 7}},
		{"oneEmpty", args{[]int{-11, 7, 2, 2, 3}, [][]int{[]int{-11, 13}, []int{}}}, []int{-11, 2, 3, 7, 13}},
		{"noneEmpty", args{[]int{-11, 7, 2, 3, 3}, [][]int{[]int{-11, 13}, []int{13, 17, 3}}}, []int{-11, 2, 3, 7, 13, 17}},
		{"more weight", args{[]int{-11, 7, 2, 3, 3}, [][]int{[]int{-11, 13}, []int{13, 17, 3}, []int{13, 19, 39}, []int{13, 79, 3}}}, []int{-11, 2, 3, 7, 13, 17, 19, 39, 79}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotAll := IntUnion(tt.args.i1, tt.args.i2...); !(reflect.DeepEqual(gotAll, tt.wantAll) || (len(gotAll) == 0 && len(tt.wantAll) == 0)) {
				t.Errorf("IntUnion() = %v, want %v", gotAll, tt.wantAll)
			}
		})
	}
}

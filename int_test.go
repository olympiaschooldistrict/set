package set

import (
	"reflect"
	"testing"
)

var intIntersectTests = []struct {
	i1    []int // input
	i2    []int // input
	left  []int // expected result
	both  []int // expected result
	right []int // expected result
}{
	{[]int{}, []int{}, []int{}, []int{}, []int{}},
	{[]int{1}, []int{2}, []int{1}, []int{}, []int{2}},
	{[]int{1, 2}, []int{2, 3}, []int{1}, []int{2}, []int{3}},
	{[]int{2, 1}, []int{3, 2}, []int{1}, []int{2}, []int{3}},
	{[]int{2}, []int{2}, []int{}, []int{2}, []int{}},
}

func TestIntIntersect(t *testing.T) {
	for _, tt := range intIntersectTests {
		l, b, r := IntIntersect(tt.i1, tt.i2)
		if !(reflect.DeepEqual(tt.left, l) || (len(tt.left) == 0 && len(l) == 0)) {
			t.Errorf("IntIntersect(%v, %v): left expected %d, actual %d", tt.i1, tt.i2, tt.left, l)
		}
		if !(reflect.DeepEqual(tt.both, b) || (len(tt.both) == 0 && len(b) == 0)) {
			t.Errorf("IntIntersect(%v, %v): both expected %d, actual %d", tt.i1, tt.i2, tt.both, b)
		}
		if !(reflect.DeepEqual(tt.right, r) || (len(tt.right) == 0 && len(r) == 0)) {
			t.Errorf("IntIntersect(%v, %v): right expected %d, actual %d", tt.i1, tt.i2, tt.right, r)
		}
	}
}

var intUnionTests = []struct {
	i1       []int   // input
	i2       [][]int // input
	expected []int   // expected result
}{
	{[]int{}, [][]int{}, []int{}},
	{[]int{1, 7, 2, 3}, [][]int{}, []int{1, 2, 3, 7}},
	{[]int{1, 7, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 7, 3}, [][]int{}, []int{1, 2, 3, 7}},
	{[]int{-11, 7, 2, 3}, [][]int{}, []int{-11, 2, 3, 7}},
	{[]int{-11, 7, 2, 2, 3}, [][]int{[]int{}, []int{}}, []int{-11, 2, 3, 7}},
	{[]int{-11, 7, 2, 2, 3}, [][]int{[]int{-11, 13}, []int{}}, []int{-11, 2, 3, 7, 13}},
	{[]int{-11, 7, 2, 3, 3}, [][]int{[]int{-11, 13}, []int{13, 17, 3}}, []int{-11, 2, 3, 7, 13, 17}},
	{[]int{-11, 7, 2, 3, 3}, [][]int{[]int{-11, 13}, []int{13, 17, 3}, []int{13, 19, 39}, []int{13, 79, 3}}, []int{-11, 2, 3, 7, 13, 17, 19, 39, 79}},
	// {[]int{1}, [][]int{2}, []int{1}, []int{}, []int{2}},
	// {[]int{1, 2}, [][]int{2, 3}, []int{1}, []int{2}, []int{3}},
	// {[]int{2, 1}, [][]int{3, 2}, []int{1}, []int{2}, []int{3}},
	// {[]int{2}, [][]int{2}, []int{}, []int{2}, []int{}},
}

func TestIntUnionDave(t *testing.T) {
	for _, tt := range intUnionTests {
		actual := IntUnion(tt.i1, tt.i2...)
		if !(reflect.DeepEqual(tt.expected, actual) || (len(tt.expected) == 0 && len(actual) == 0)) {
			t.Errorf("IntUnion(%v, %v):  expected %d, actual %d", tt.i1, tt.i2, tt.expected, actual)
		}
	}
}
